#!/usr/bin/env python3

import os
import socket
import sys
import struct
import queue
import scrypt
import threading

#Created with help from Paradis

#Creates the queue that will hold all the threads
q = queue.Queue()

class Packet_header:
	"""Class that is created to store the header information that will be sent"""

	#Is the fields in the packet provided by Liam
	def __init__(self, type1, size, custom):
		self.type1 = type1
		self.size = size
		self.custom = custom

	#Function that returns the packed information
	def serialize(self):
		return struct.pack("!HHL", self.type1, self.size, self.custom)

def worker():
	"""Function that grabs from the queue and then sends to the sludger function"""

	while True:
		item = q.get()
		if item is None:
		    break
		sludger(item)
		q.task_done()

def sludger(data):
	"""Found scrypt methods at https://pypi.python.org/pypi/scrypt/"""

	#print(data)
	if data:
		#Create the socket
		out = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		
		#Create an empty list and then split the data sent to the sludger function based on a ','
		listo = []
		listo = data.split(',')

		header = Packet_header(2, 8 + len(listo)*64, 0)

		h1 = b''
		h1 += header.serialize()

		#Loop through the list and set everything to a string and then create the salt provided by Liam
		for i in listo:
			i = str(i)
			#print(i)
			salt  = "I Hate Liam Echlin"
			h1 += scrypt.hash(i, salt, N = 2048, r = 4, p = 4)

		sent = 1
		#While sent is true continue trying to connect to the downstream on port 4444 in order to send
		#the information
		while sent:
			try:
				out.connect(("downstream", 4444))
				out.send(h1)
				sent = 0
			except Exception:
				continue
		
		#Close the socket	
		out.close()

def main():
	"""Main function that spawns worker threads and connects to the provided port in order to grab the neccessary
	data that will eventually get sent to the sludger function"""

	num_of_threads = 20

	server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

	server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

	server.bind(('127.0.0.1', 40001))	

	server.listen(num_of_threads)

	threads = []

	#Loop through the threads and append them to the threads list
	for i in range(num_of_threads):
		t = threading.Thread(target=worker)
		t.start()
		threads.append(t)

	collected_data = []	

	#While server try to accept a connection and if there was a successful connection recieve the data and appended it to the collected_data list,
	#otherwise if an error occurs to join the queue and threads and then close the server
	while server:
		try:
			conn, addr = server.accept()
			if conn:
				try:
					conn.settimeout(5)
					val = conn.recv(1024).decode('utf-8')
					#print(val)
					collected_data.append(val)
				except socket.timeout:
					pass

			#Goes through the collected_data list and puts each piece of data in the queue, then removing that data from the collected_data list
			for data in collected_data:
				q.put(data)
				collected_data.remove(data)
		except Exception:
			#Should block while tasks are not done
			q.join()

			#Loops through the num_of_threads to eventually join them all
			for i in range(num_of_threads):
				q.put(None)
			for t in threads:
				t.join()	
			
			#Closes the server
			server.close()

if __name__ == "__main__":
	main()
	
