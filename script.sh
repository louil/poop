#!/bin/bash

PID_FILE=/var/run/rerun.pid # need root permission to store in this directory
EXEC=/root/poop/sludge_server.py # replace it with actual executable 
EXEC1=/root/poop/waste_server.py
EXEC2=/root/poop/residential.py

function run() {
    # execute the program
    $EXEC &
    $EXEC1 &
    $EXEC2 &
    # save its PID
    echo $! > $PID_FILE
}

if [ -e $PID_FILE ]; then
    # check if program is still running
    pid=$(<$PID_FILE)

    # find the proper process
    ps --pid $pid|grep -q `basename $EXEC`

    if [ $? != 0 ]; then
        # not found - rerun
        run
    fi
else
    # no PID file - just execute
    run
fi
