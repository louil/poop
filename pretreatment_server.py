#!/usr/bin/env python3

import os
import socket
import sys
import select
import threading
import math
import struct
import queue
import scrypt

#Created with help from Primm and Paradis

#Creates the queue that will hold all the threads
q = queue.Queue()

#Created three buckets that will store data to be sent off 
sludge_bucket = []
waste_bucket = []
water_bucket = []

class Packet_header:
	#Is the fields in the packet provided by Liam
	def __init__(self, type1, size, custom):
		self.type1 = type1
		self.size = size
		self.custom = custom
	
	#Function that returns the packed information
	def serialize(self):
		return struct.pack("!HHL", self.type1, self.size, self.custom)

#Class to create a bucket
class Bucket:
	def __init__(self):
		self.data = None
		self.lock = None
	
	#Function in the class that will append data to the sludge_bucket
	def add_sludge(self, data):
		self.lock.acquire()
		sludge_bucket.append(data)
		self.lock.release()
	
	#Function in the class that will append data to the waste_bucket
	def add_waste(self, data):
		self.lock.acquire()
		waste_bucket.append(data)
		self.lock.release()

	#Function in the class that will append data to the water_bucket
	def add_water(self, data):
		self.lock.acquire()
		water_bucket.append(data)
		self.lock.release()

#Found at http://stackoverflow.com/questions/15285534/isprime-function-for-python-language
def is_prime(n):
	if n==2 or n==3: return True
	if n%2==0 or n<2: return False
	for i in range(3,int(n**0.5)+1,2):   # only odd numbers
		if n%i==0:
			return False    

	return True

#Found at http://codegolf.stackexchange.com/questions/3134/undulant-numbers?page=1&tab=votes#tab-top
def is_undulant(n):
	if n == 0:
		return False

	digits = [int(i) for i in list(str(n))]
	diffs = []
	for i in range(len(digits) - 1):
		diffs.append((digits[i] > digits[i+1]) - (digits[i] < digits[i+1]))
	if len(diffs) == 1:
	   if diffs[0] == 0:
		   return False
	else:
	   for i in range(len(diffs) - 1):
		   if diffs[i] * diffs[i+1] != -1:
		       return False
	return True

#Found at http://hubpages.com/education/How-to-Tell-If-a-Number-is-Triangular
def is_triangular(num):
	if num == 0:
		return False

	answer = 0.5*math.sqrt(8*num + 1) - 0.5

	if answer.is_integer():
		return True
	else:
		return False

#Derived from primmm
def is_fibonacci(data):
	fib_numbers = {0:0,1:1,2:1,3:2,4:3,5:5,6:8,7:13,8:21,9:34,10:55,11:89,
				12:233,14:377,15:610,16:987,17:1597,18:2584,19:4181,20:6765,
				21:10946,22:17711,23:28657,24:46368,25:75025,26:121393,27:196418,28:317811,
				29:514229,30:832040,31:1346269,32:2178309,33:3524578,34:5702887,35:9227465,36:14930352,
				37:24157817,38:39088169,39:63245986,40:102334155,41:165580141,42:267914296,43:433494437,
				44:701408733,45:1134903170,46:1836311903,47:2971215073,48:4807526976
				}

	if data in fib_numbers.values():
		return data
	else:
		return 0

#Derived from Primm
def molecules(code):
	pull = 8
	(type, size, coustom) = struct.unpack("!HHL", code[:pull])
	molecules = [""]
	while pull < len(code):
		(data, left, right) = struct.unpack("!LHH", code[pull:pull+8])
		molecules.append((left, right, data))
		pull += 8

	ret_list = generate_mols(molecules)
	return ret_list

#Derived from Primm
def rip(md, a):
	i = 1
	while True:
		if i > len(a) - 1:
			return
		if i not in md.keys():
			if a[i][0] in md.keys():
				#print(a[i][0], i)
				md[i] = (a[i][0], a[i][1])
			i += 1
			continue
		if md[i][0] not in md.keys():
			try:
				md[md[i][0]] = (a[md[i][0]][0], a[md[i][0]][1])
			except Exception:
				pass
		if md[i][1] not in md.keys():
			try:
				md[md[i][1]] = (a[md[i][1]][0], a[md[i][1]][1])
			except Exception:
				pass
		i += 1

#Derived from Primm
def generate_mols(molecules):
	m_list = []
	while True:
		x = 1
		md = {}
		while True:
			if x in m_list:
				x += 1
				continue
			elif x + 1 > len(molecules):
				break
			if molecules[x][0] or molecules[x][1]:
				md[x] = (molecules[x][0], molecules[x][1])
				break
			else:
				x += 1
		rip(md, molecules)
		for i in md:
			m_list.append(i)
		m_list.append("")
		if x + 1 > len(molecules):
			break
	ret_list = []
	i_list = [(0,0,0) for x in range(len(molecules))]
	added = 0
	for i in m_list:
		if i:
			added = 1
			i_list[i] = molecules[i]
			if molecules[i][0] > len(i_list) or molecules[i][1] > len(i_list):
				i_list[0] = 1
		else:
			if added:
				ret_list.append(i_list)
				added = 0
			i_list = [(0,0,0) for x in range(len(molecules))]
	return ret_list

def worker():
	while True:
		item = q.get()
		if item is None:
		    break
		parser(*item)
		#print("HAHAHAHA")
		q.task_done()

#Function that goes through the item that is recieved and creates sockets that connect to the sludge_server and waste_server.
#A dictionary holding the different functions to be performed upon the item recieved are sorted in the order that is provided by Liam.
def parser(item, ip_address):
	lock = threading.Semaphore()

	#The bucket class is created under the name bucket
	bucket = Bucket()

	bucket.lock = lock

	sludge_outgoing = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sludge_outgoing.connect(('127.0.0.1', 40001))

	waste_outgoing = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	waste_outgoing.connect(('127.0.0.1', 40002))

	functions = {"0": bacteria, "1": debris, "2": mercury, "3": selenium,
	         "4": feces, "5": ammonia, "6": deaeration,
			"7": chlorine, "8": lead}

	mol = []
	p_l = []
	water_list = []

	mol = molecules(item)
	#print("The is mol at the beggining {}".format(mol))

	for i in mol:
		p_l = functions["0"](i, bucket)
		if p_l:
			i = p_l
			type1 = 4
			report(type1, ip_address)

		p_l = functions["1"](i)
		if p_l:
			out = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

			header = Packet_header(1, 8 + 8*len(p_l), 0)

			h1 = b''
			h1 += header.serialize()

			for i in p_l:
				h1 += struct.pack("!LHH", i[2], i[0], i[1])

			sent = 1
			while sent:
				try:
					out.connect(("downstream", 2222))
					header = Packet_header(1, 8 + 8*len(p_l), 0)
					type1 = 1
					report(type1, ip_address)
					out.send(h1)
					sent = 0
				except Exception:
					continue

			out.close()
			return 0

		p_l = functions["2"](i, bucket)
		if p_l:
			i = p_l
			type1 = 4
			report(type1, ip_address)
			#print("This be i after mercury is called {}".format(i))

		p_l = functions["3"](i, bucket)
		if p_l:
			i = p_l
			type1 = 4
			report(type1, ip_address)
			#print("This be i after selenium is called {}".format(i))

		p_l = functions["4"](i, bucket)
		if p_l:
			i = p_l
			type1 = 2
			report(type1, ip_address)
			#print("This be i after feces is called {}".format(i))
	
		p_l = functions["5"](i, bucket)
		if p_l:
			i = p_l
			type1 = 2
			report(type1, ip_address)
			#print("This be i after ammonia is called {}".format(i))

		p_l = functions["6"](i)
		if p_l:
			i = p_l

		#print("This is p_l before lead is called {}".format(p_l))
		p_l = functions["8"](i, bucket)
		if p_l:
			i = p_l
			type1 = 4
			report(type1, ip_address)	
		
		if i:
			water_list = i	

	lock.acquire()
	#sludge_bucket = str(sludge_bucket)
	if len(sludge_bucket) in range(10, 20):
		sludge_outgoing.send(bytes(','.join(sludge_bucket),'utf-8'))
		sludge_bucket[:] = []
	lock.release()
	sludge_outgoing.close()

	lock.acquire()
	#waste_bucket = str(waste_bucket)
	if len(waste_bucket) in range(10, 20):
		waste_outgoing.send(bytes(','.join(waste_bucket),'utf-8'))
		waste_bucket[:] = []
	lock.release()
	waste_outgoing.close()

	if water_list:
		for i in water_list:
			if i[2]:
				bucket.add_water(i)	

		#water_bucket = str(water_bucket)
		print("Length of water bucket is {}".format(len(water_bucket)))
	
		lock.acquire()

		if len(water_bucket) > 200:
			print("Sending water")
			water = functions["7"](water_bucket)
			
			header = Packet_header(0, 8 + 8*len(water_bucket), 0)
			h1 = b''

			h1 += header.serialize()			

			for i in water:
				h1 += struct.pack("!LHH", i[2], i[0], i[1])
		
			sent = 1
			water_outgoing = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			while sent:
				try:
					water_outgoing.connect(('downstream', 1111))
					water_outgoing.send(h1)
					sent = 0
					water_bucket[:] = []
				except e as Exception:
					print(e)
					continue

			water_outgoing.close()

		lock.release()

def report(type1, ip_address):
	if type1 == 1:
		message = "Debris found in stormdrain"
	elif type1 == 2:
		message = "Sludge found in stormdrain"
	elif type1 == 4:
		message = "Hazmat found in stormdrain"

	if len(message) < 56:
		temp_message = 56 - len(message)
		temp_message = '\0' * temp_message
		message += temp_message

	message = bytes(message, "ascii")
	message = list(message)

	out = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	out.connect(("downstream", 9999))
	header = Packet_header(8, 8+64, 0)
	h1 = b''
	h1 += header.serialize()
	h1 += struct.pack("!HH4s56B", 16, 0, socket.inet_pton(socket.AF_INET, ip_address[0]), *message)
	out.send(h1)
	out.close()

def bacteria(p_list, bucket):
	listo = []
	fung = 0
	for mol in p_list[1:]:
		left = mol[0]
		right = mol[1]
		data = mol[2]

		if is_fibonacci(data):
			print("Found fungus/bacteria")
			bucket.add_waste(str(data))
			data = 0
			fung = 1
		else:
			pass

		listo.append((left, right, data))

	if fung:
		return listo
	else:
		return 0

#Derived from primmm
def clean_debris(p_list):
	lenlen = len(p_list)
	ret_list = []
	for mol in p_list[1:]:
		left = mol[0]
		right = mol[1]
		data = mol[2]
		if not data:
			continue
		if left > lenlen:
			left = 0xFFFF
		if right > lenlen:
			right = 0xFFFF
		ret_list.append((left, right, data))
	return ret_list

#Derived from primmm
def debris(p_list):
	lenlen = len(p_list)
	for mol in p_list[1:]:
		left = mol[0]
		right = mol[1]
		data = mol[2]
		if left > lenlen:
			p_list = clean_debris(p_list)
			return p_list
		elif right > lenlen:
			p_list = clean_debris(p_list)
			return p_list
	return 0

#Derived from PRimm
def mercury(p_list, bucket):
	dll = []
	for mol in p_list:
		left = mol[0]
		right = mol[1]
		data = mol[2]
		dll.append((left, right, data))
	change = 0
	while True:
		if z_check(dll, bucket) <= 1:
			break
		change = 1
	if change:
		#print("This is dll in mercury before returning {}".format(dll))
		return dll
	return dll

def z_check(dll, bucket):
	dic = {}
	m_set = set()
	for i in range(1, len(dll)):
		if dll[i] != (0,0,0):
			dic[i] = dll[i]
			m_set.add(dll[i][0])
			m_set.add(dll[i][1])
	zpoint = 0
	if 0 in m_set:
		m_set.remove(0)
	for i in dic:
		#print(zpoint)
		if i not in m_set:
			zpoint += 1
	if zpoint > 1:
		clean_mercury(dll, dic, m_set, bucket)
	return zpoint

def clean_mercury(dll, dic, m_set, bucket):
	print("Found mercury")
	small = 0
	trash = None
	for i in dic:
		if i in m_set:
			continue
		else:
			if not trash:
				small = dll[i][2] 
				trash = i
			elif small > dll[i][2]:
				small = dll[i][2] 
				trash = i
	#waste(dll[trash])
	#print("Inside of clean_mercury printing dll[trash] {}".format(dll[trash][2]))
	bucket.add_waste(str(dll[trash][2]))
	dll[trash] = (0,0,0)
	#print(dll)

#Derived from Primm
def selenium(p_list, bucket):
	#print(p_list)
	lenlen = len(p_list)
	dll = [('')]
	s_child = 0
	for mol in p_list:
		left = mol[0]
		right = mol[1]
		data = mol[2]
		if left == 0 and right == 0:
			continue
		dll.append((left, right, data))
	l = 0
	r = 0
	m_dict = {}
	for i in range(1, len(dll)):
		m_dict[i] = (dll[i][0], dll[i][1], dll[i][2])
	# checking for dbl circle
	chain = 0
	dbl = 0
	#print("This is the dict {}".format(m_dict))
	for i in m_dict:
		#print("This is i {}".format(i))
		if m_dict[i][0] == 0 and m_dict[i][1] or m_dict[i][1] == 0 and m_dict[i][0] or m_dict[i][0] == m_dict[i][1]:
			if dbl:
				return 0  # this cannot be selenium
			chain = 1
		if m_dict[i][0] == 0 and m_dict[i][0] == 0 and m_dict[i][2]:
			return 0
		else:
			if chain:
				return 0  # this cannot be selenium
			dbl = 1
	
	if chain:
		p_list = clean_chain(m_dict, bucket)
		return p_list
	else:
		#print(dll)
		p_list = clean_dbl(m_dict, bucket)
		return p_list		

#Derived from Primm	
def clean_dbl(dll, bucket):
	#print("This is dbl")
	big = 0
	ret_list = []
	last = 0
	for i in dll:
		if big < dll[i][2]:
			big = dll[i][2]
			trash = i
	for i in dll:
		if not last:
			last = i
		#print(last)
		if i in dll[[i][0]] and i in dll[[i][1]]:
			pass
		else:
			return 0
		if i == trash:
			ret_list.append((dll[i][0], dll[i][1], 0))
		elif trash == dll[i][0]:
			if last == dll[i][1]:
				ret_list.append((0, 0, dll[i][2]))
			else:
				ret_list.append((0, dll[i][1], dll[i][2]))
		elif trash == dll[i][1]:
			if last == dll[i][0]:
				ret_list.append((0, 0, dll[i][2]))
			else:
				ret_list.append((dll[i][0], 0, dll[i][2]))
		else:
			if i in [dll[i][0]]:
				ret_list.append((dll[i][0], 0, dll[i][2]))
			else:
				ret_list.append((dll[i][1], 0, dll[i][2]))
		last = i
	print("Found dbl selanium")
	#waste(dll[trash][2])
	bucket.add_waste(str(dll[trash][2]))
	send_list = []
	for item in ret_list:
		send_list.append((item[0], item[1], item[2]))
	return send_list

#Derived from Primm
def clean_chain(dll, bucket):
	#print("This is chain")
	last = 0
	big = 0
	for i in dll:
		if type(i) == int:
			if dll[i][2] > big:
				big = dll[i][2]
				trash = i
	ret_list = []
	for i in dll:
		if dll[i][0] == i:
			if dll[i][1] not in dll.keys():
				return 0
		if dll[i][1] == i:
			if dll[i][0] not in dll.keys():
				return 0
	print("Found chain selanium")
	for i in dll:
		#print(i, trash, dll[i])
		if i == trash:
			ret_list.append((dll[i][0], dll[i][1], 0))
		elif trash == dll[i][0]:
			if trash == dll[i][1]:
				ret_list.append((0, 0, dll[i][2]))
			else:
				ret_list.append((0, dll[i][1], dll[i][2]))
		elif trash == dll[i][1]:
			ret_list.append((dll[i][0], 0, dll[i][2]))
		else:
			ret_list.append(dll[i])
	#waste(dll[trash][2])
	bucket.add_waste(str(dll[trash][2]))
	send_list = []
	for item in ret_list:
		send_list.append((item[0], item[1], item[2]))
	return send_list

def feces(p_list, bucket):
	listo = []
	poo = 0
	for mol in p_list:
		#print("Here be mol in feces {}".format(mol))
		left = mol[0]
		right = mol[1]
		data = mol[2]
		if is_prime(data):
			print("Found feces")
			bucket.add_sludge(str(data))
			data = 0
			poo = 1
		else:
			pass
		listo.append((left, right, data))	

	if poo:
		return listo
	else:
		return 0

def ammonia(p_list, bucket):
	listo = []
	pee = 0
	for mol in p_list:
		#print("Here be mol in ammonia {}".format(mol))
		left = mol[0]
		right = mol[1]
		data = mol[2]
		if is_undulant(data):
			print("Found ammonia")
			bucket.add_sludge(str(data))
			data = 0
			pee = 1
		else:
			pass
		#print("This is data before appending {}".format(data))
		listo.append((left, right, data))	
		#print("Here be mol in ammonia after {}".format(listo))
	if pee:
		return listo
	else:
		return 0

def deaeration(p_list):
	dll = []
	for mol in p_list:
		left = mol[0]
		right = mol[1]
		data = mol[2]
		dll.append((left, right, data))

	change = 0
	while True:
		if air_check(dll) == 0:
			break
		change = 1
	if change:
		return dll
	return 0

def air_check(dll):
	dic = {}
	for i in range(1, len(dll)):
		if dll[i][2] == 0:
			clean_all_zero(dll)
			return 1
	return 0

def clean_all_zero(dll):
	remove = None
	for i in range(1, len(dll)):
		if dll[i][2] == 0:
			remove = i
			break
	for i in range(1, len(dll)):
		if i == remove:
			continue
		if dll[i][0] == remove:
			dll[i] = (dll[dll[i][0]][0], dll[i][1],dll[i][2])
		if dll[i][1] == remove:
			dll[i] = (dll[i][0], dll[dll[i][1]][1],dll[i][2])
		if dll[i][0] >= remove:
			dll[i] = (dll[i][0]-1, dll[i][1],dll[i][2])
		if dll[i][1] >= remove:
			dll[i] = (dll[i][0], dll[i][1]-1,dll[i][2])
	#print(dll)
	dll.pop(remove)
	#print(remove)

def correct_water_bucket(bucket):
	last_num = len(bucket)-11
	listo = []
	num = 0	

	for mol in bucket:
		left = mol[0]
		right = mol[1]
		data = mol[2]

		if num > last_num:
			listo.append((left, right, data))
			continue

		if num == 0:
			left = 0
			right = num + 1
		else:
			left = 0
			right = num + 1

		num += 1

		listo.append((left, right, data))

	return listo

def chlorine(bucket):
	num = len(bucket)-9
	for mol in bucket[0:8]:
		left = mol[0]
		right = mol[1]
		data = mol[2]

		left = num+2
		right = num+2
	
		bucket.append((left, right, data))
	
		num += 1

	bucket.append((0, 0, 0))
	bucket.append((0, 0, 0))

	for i in bucket[0:8]:
		bucket.remove(i)

	correct_bucket = correct_water_bucket(bucket)

	return correct_bucket

def lead(p_list, bucket):
	dll = []
	lead = 0
	#print("Here is the god dam p_list in lead {}".format(p_list))
	for mol in p_list:
		left = mol[0]
		right = mol[1]
		data = mol[2]
		if is_triangular(data):
			print("Found lead")
			bucket.add_waste(str(data))
			data = 0
			lead = 1
		else:
			pass

		dll.append((left, right, data))	

	if lead:
		return dll
	else:
		return 0

def main():
	num_worker_threads = 4

	server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

	server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

	server.bind(('', 1111))	

	server.listen(num_worker_threads)

	threads = []
	for i in range(num_worker_threads):
		t = threading.Thread(target=worker)
		t.start()
		threads.append(t)

	recieved = []	

	while server:
		try:
			if len(threads) < num_worker_threads:
				t = threading.Thread(target=worker)
				t.start()
				threads.append(t)

			try:
				conn, addr = server.accept()
			except Exception:
					break

			if conn:
				try:
					conn.settimeout(5)
					val = conn.recv(1024)
					connection_tuple = (val, addr);
					recieved.append(connection_tuple)
				except socket.timeout:
					pass

			for item in recieved:
				q.put(item)
				recieved.remove(item)
		except Exception:
			# block until all tasks are done
			q.join()

			# stop workers
			for i in range(num_worker_threads):
				q.put(None)
			for t in threads:
				t.join()
	
if __name__ == "__main__":
	main()
