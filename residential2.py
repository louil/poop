#!/usr/bin/env python3

import os
import socket
import sys
import select
import threading
import math
import struct
import queue
import scrypt

q = queue.Queue()

sludge_bucket = []
waste_bucket = []
water_bucket = []

class Header:
	def __init__(self, type1, size, custom):
		self.type1 = type1
		self.size = size
		self.custom = custom

	def serialize(self):
		return struct.pack("!HHL", self.type1, self.size, self.custom)

class Bucket:
	def __init__(self):
		self.data = None
	
	def add(self, data):
		#self.data = data
		#print(data)
		sludge_bucket.append(data)
	
	def add_waste(self, data):
		#print(data)
		waste_bucket.append(data)

	def add_water(self, data):
		water_bucket.append(data)

def is_prime(n):
	if n==2 or n==3: return True
	if n%2==0 or n<2: return False
	for i in range(3,int(n**0.5)+1,2):   # only odd numbers
		if n%i==0:
			return False    

	return True

#Found at http://codegolf.stackexchange.com/questions/3134/undulant-numbers?page=1&tab=votes#tab-top
def is_undulant(n):
	if n == 0:
		return False

	digits = [int(i) for i in list(str(n))]
	diffs = []
	for i in range(len(digits) - 1):
		diffs.append((digits[i] > digits[i+1]) - (digits[i] < digits[i+1]))
	if len(diffs) == 1:
	   if diffs[0] == 0:
		   return False
	else:
	   for i in range(len(diffs) - 1):
		   if diffs[i] * diffs[i+1] != -1:
		       return False
	return True

#Derived from Primm
def is_triangular(num):
	if num == 0:
		return False

	num = num * 2
	n = int(math.sqrt(num))
	if n ** 2 + n == num:
		return True
	elif (n+1) ** 2 + (n+1) == num:
		return True
	return False

def molecules(code):
	pull = 8
	(type, size, coustom) = struct.unpack("!HHL", code[:pull])
	molecules = [""]
	while pull < len(code):
		(data, left, right) = struct.unpack("!LHH", code[pull:pull+8])
		molecules.append((left, right, data))
		pull += 8

	ret_list = generate_mols(molecules)
	return ret_list

def rip(md, a):
	i = 1
	while True:
		if i > len(a) - 1:
			return
		if i not in md.keys():
			if a[i][0] in md.keys():
				#print(a[i][0], i)
				md[i] = (a[i][0], a[i][1])
			i += 1
			continue
		if md[i][0] not in md.keys():
			try:
				md[md[i][0]] = (a[md[i][0]][0], a[md[i][0]][1])
			except Exception:
				pass
		if md[i][1] not in md.keys():
			try:
				md[md[i][1]] = (a[md[i][1]][0], a[md[i][1]][1])
			except Exception:
				pass
		i += 1

def generate_mols(molecules):
	m_list = []
	while True:
		x = 1
		md = {}
		while True:
			if x in m_list:
				x += 1
				continue
			elif x + 1 > len(molecules):
				break
			if molecules[x][0] or molecules[x][1]:
				md[x] = (molecules[x][0], molecules[x][1])
				break
			else:
				x += 1
		rip(md, molecules)
		for i in md:
			m_list.append(i)
		m_list.append("")
		if x + 1 > len(molecules):
			break
	ret_list = []
	i_list = [(0,0,0) for x in range(len(molecules))]
	added = 0
	for i in m_list:
		if i:
			added = 1
			i_list[i] = molecules[i]
			if molecules[i][0] > len(i_list) or molecules[i][1] > len(i_list):
				i_list[0] = 1
		else:
			if added:
				ret_list.append(i_list)
				added = 0
			i_list = [(0,0,0) for x in range(len(molecules))]
	return ret_list

def worker():
	while True:
		item = q.get()
		if item is None:
		    break
		parser(item)
		#print("HAHAHAHA")
		q.task_done()

def parser(item):
	bucket = Bucket()
	sludge_outgoing = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sludge_outgoing.connect(('127.0.0.1', 40001))

	waste_outgoing = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	waste_outgoing.connect(('127.0.0.1', 40002))

	functions = {"1": debris, "2": mercury, "3": selenium,
	         "4": feces, "5": ammonia, "6": deaeration, "7": phosphates,
			"8": chlorine, "9": lead}

	mol = []
	p_l = []
	water_list = []

	mol = molecules(item)
	#print("The is mol at the beggining {}".format(mol))

	for i in mol:
		p_l = functions["1"](i)
		if p_l:
			outgoing = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

			h1 = b''
			for i in p_l:
				h1 += struct.pack("!LHH", i[2], i[0], i[1])

			sent = 1
			while sent:
				try:
					outgoing.connect(("downstream", 2222))
					header = Header(1, 8 + 8*len(p_l), 0)
					outgoing.send(header.serialize())
					outgoing.send(h1)
					sent = 0
				except Exception:
					continue

			outgoing.close()
			return 0

		p_l = functions["2"](i, bucket)
		if p_l:
			i = p_l
			#print("This be i after mercury is called {}".format(i))

		p_l = functions["3"](i, bucket)
		if p_l:
			i = p_l
			#print("This be i after selenium is called {}".format(i))

		p_l = functions["4"](i, bucket)
		if p_l:
			i = p_l
			#print("This be i after feces is called {}".format(i))
	
		p_l = functions["5"](i, bucket)
		if p_l:
			i = p_l
			#print("This be i after ammonia is called {}".format(i))

		p_l = functions["6"](i)
		if p_l:
			i = p_l

		#p_l = functions["7"](i, bucket)
		#if p_l:
			#i = p_l

		p_l = functions["8"](i)
		if p_l:
			i = p_l

		#print("This is p_l before lead is called {}".format(p_l))
		p_l = functions["9"](i, bucket)
		if p_l:
			i = p_l		
		
		if i:
			water_list = i	

	sludge_bucket = str(sludge_bucket)
	if len(sludge_bucket) in range(10, 20):
		sludge_outgoing.send(bytes(','.join(sludge_bucket),'utf-8'))
		sludge_bucket[:] = []
	
	#sludge_outgoing.close()

	#waste_bucket = str(waste_bucket)
	if len(waste_bucket) in range(10, 20):
		waste_outgoing.send(bytes(','.join(waste_bucket),'utf-8'))
		waste_bucket[:] = []
	
	waste_outgoing.close()

	if water_list:
		for i in water_list:
			if i[2]:
				bucket.add_water(i)	

		#water_bucket = str(water_bucket)
		print("Length of water bucket is {}".format(len(water_bucket)))
		if len(water_bucket) > 95:
			print("Sending water")

			h1 = b''
			for i in water_bucket:
				h1 += struct.pack("!LHH", i[2], i[0], i[1])
				
			sent = 1
			water_outgoing = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			while sent:
				try:
					water_outgoing.connect(('downstream', 1111))
					header = Header(0, 8 + 8*len(water_list), 0)
					water_outgoing.send(header.serialize())
					water_outgoing.send(h1)
					sent = 0
					water_bucket[:] = []
				except Exception:
					continue

			water_outgoing.close()

#Derived from primmm
def clean_debris(p_list):
	lenlen = len(p_list)
	ret_list = []
	for mol in p_list[1:]:
		left = mol[0]
		right = mol[1]
		data = mol[2]
		if not data:
			continue
		if left > lenlen:
			left = 0xFFFF
		if right > lenlen:
			right = 0xFFFF
		ret_list.append((left, right, data))
	return ret_list

#Derived from primmm
def debris(p_list):
	lenlen = len(p_list)
	for mol in p_list[1:]:
		left = mol[0]
		right = mol[1]
		data = mol[2]
		if left > lenlen:
			p_list = clean_debris(p_list)
			return p_list
		elif right > lenlen:
			p_list = clean_debris(p_list)
			return p_list
	return 0

#Derived from PRimm
def mercury(p_list, bucket):
	dll = []
	for mol in p_list:
		left = mol[0]
		right = mol[1]
		data = mol[2]
		dll.append((left, right, data))
	change = 0
	while True:
		if z_check(dll, bucket) <= 1:
			break
		change = 1
	if change:
		#print("This is dll in mercury before returning {}".format(dll))
		return dll
	return dll

def z_check(dll, bucket):
	dic = {}
	m_set = set()
	for i in range(1, len(dll)):
		if dll[i] != (0,0,0):
			dic[i] = dll[i]
			m_set.add(dll[i][0])
			m_set.add(dll[i][1])
	zpoint = 0
	if 0 in m_set:
		m_set.remove(0)
	for i in dic:
		#print(zpoint)
		if i not in m_set:
			zpoint += 1
	if zpoint > 1:
		clean_mercury(dll, dic, m_set, bucket)
	return zpoint

def clean_mercury(dll, dic, m_set, bucket):
	print("Found mercury")
	small = 0
	trash = None
	for i in dic:
		if i in m_set:
			continue
		else:
			if not trash:
				small = dll[i][2] 
				trash = i
			elif small > dll[i][2]:
				small = dll[i][2] 
				trash = i
	#waste(dll[trash])
	#print("Inside of clean_mercury printing dll[trash] {}".format(dll[trash][2]))
	bucket.add_waste(str(dll[trash][2]))
	dll[trash] = (0,0,0)
	#print(dll)

#Derived from Primm
def selenium(p_list, bucket):
	#print(p_list)
	lenlen = len(p_list)
	dll = [('')]
	s_child = 0
	for mol in p_list:
		left = mol[0]
		right = mol[1]
		data = mol[2]
		if left == 0 and right == 0:
			continue
		dll.append((left, right, data))
	l = 0
	r = 0
	m_dict = {}
	for i in range(1, len(dll)):
		m_dict[i] = (dll[i][0], dll[i][1], dll[i][2])
	# checking for dbl circle
	chain = 0
	dbl = 0
	#print("This is the dict {}".format(m_dict))
	for i in m_dict:
		#print("This is i {}".format(i))
		if m_dict[i][0] == 0 and m_dict[i][1] or m_dict[i][1] == 0 and m_dict[i][0] or m_dict[i][0] == m_dict[i][1]:
			if dbl:
				return 0  # this cannot be selenium
			chain = 1
		if m_dict[i][0] == 0 and m_dict[i][0] == 0 and m_dict[i][2]:
			return 0
		else:
			if chain:
				return 0  # this cannot be selenium
			dbl = 1
	
	if chain:
		p_list = clean_chain(m_dict, bucket)
		return p_list
	else:
		#print(dll)
		p_list = clean_dbl(m_dict, bucket)
		return p_list		

#Derived from Primm	
def clean_dbl(dll, bucket):
	#print("This is dbl")
	big = 0
	ret_list = []
	last = 0
	for i in dll:
		if big < dll[i][2]:
			big = dll[i][2]
			trash = i
	for i in dll:
		if not last:
			last = i
		#print(last)
		if i in dll[[i][0]] and i in dll[[i][1]]:
			pass
		else:
			return 0
		if i == trash:
			ret_list.append((dll[i][0], dll[i][1], 0))
		elif trash == dll[i][0]:
			if last == dll[i][1]:
				ret_list.append((0, 0, dll[i][2]))
			else:
				ret_list.append((0, dll[i][1], dll[i][2]))
		elif trash == dll[i][1]:
			if last == dll[i][0]:
				ret_list.append((0, 0, dll[i][2]))
			else:
				ret_list.append((dll[i][0], 0, dll[i][2]))
		else:
			if i in [dll[i][0]]:
				ret_list.append((dll[i][0], 0, dll[i][2]))
			else:
				ret_list.append((dll[i][1], 0, dll[i][2]))
		last = i
	print("Found dbl selanium")
	#waste(dll[trash][2])
	bucket.add_waste(str(dll[trash][2]))
	send_list = []
	for item in ret_list:
		send_list.append((item[0], item[1], item[2]))
	return send_list

#Derived from Primm
def clean_chain(dll, bucket):
	#print("This is chain")
	last = 0
	big = 0
	for i in dll:
		if type(i) == int:
			if dll[i][2] > big:
				big = dll[i][2]
				trash = i
	ret_list = []
	for i in dll:
		if dll[i][0] == i:
			if dll[i][1] not in dll.keys():
				return 0
		if dll[i][1] == i:
			if dll[i][0] not in dll.keys():
				return 0
	print("Found chain selanium")
	for i in dll:
		#print(i, trash, dll[i])
		if i == trash:
			ret_list.append((dll[i][0], dll[i][1], 0))
		elif trash == dll[i][0]:
			if trash == dll[i][1]:
				ret_list.append((0, 0, dll[i][2]))
			else:
				ret_list.append((0, dll[i][1], dll[i][2]))
		elif trash == dll[i][1]:
			ret_list.append((dll[i][0], 0, dll[i][2]))
		else:
			ret_list.append(dll[i])
	#waste(dll[trash][2])
	bucket.add_waste(str(dll[trash][2]))
	send_list = []
	for item in ret_list:
		send_list.append((item[0], item[1], item[2]))
	return send_list

def feces(p_list, bucket):
	listo = []
	poo = 0
	for mol in p_list:
		#print("Here be mol in feces {}".format(mol))
		left = mol[0]
		right = mol[1]
		data = mol[2]
		if is_prime(data):
			print("Found feces")
			bucket.add(str(data))
			print("This is waste_bucket in feces {}".format(waste_bucket))
			data = 0
			poo = 1
		else:
			pass
		listo.append((left, right, data))	

	if poo:
		return listo
	else:
		return 0

def ammonia(p_list, bucket):
	listo = []
	pee = 0
	for mol in p_list:
		#print("Here be mol in ammonia {}".format(mol))
		left = mol[0]
		right = mol[1]
		data = mol[2]
		if is_undulant(data):
			print("Found ammonia")
			bucket.add(str(data))
			print("This is waste_bucket in ammonia {}".format(waste_bucket))
			data = 0
			pee = 1
		else:
			pass
		#print("This is data before appending {}".format(data))
		listo.append((left, right, data))	
		#print("Here be mol in ammonia after {}".format(listo))
	if pee:
		return listo
	else:
		return 0

def deaeration(p_list):
	dll = []
	for mol in p_list:
		left = mol[0]
		right = mol[1]
		data = mol[2]
		dll.append((left, right, data))

	change = 0
	while True:
		if air_check(dll) == 0:
			break
		change = 1
	if change:
		return dll
	return 0

def air_check(dll):
	dic = {}
	for i in range(1, len(dll)):
		if dll[i][2] == 0:
			clean_all_zero(dll)
			return 1
	return 0

def clean_all_zero(dll):
	remove = None
	for i in range(1, len(dll)):
		if dll[i][2] == 0:
			remove = i
			break
	for i in range(1, len(dll)):
		if i == remove:
			continue
		if dll[i][0] == remove:
			dll[i] = (dll[dll[i][0]][0], dll[i][1],dll[i][2])
		if dll[i][1] == remove:
			dll[i] = (dll[i][0], dll[dll[i][1]][1],dll[i][2])
		if dll[i][0] >= remove:
			dll[i] = (dll[i][0]-1, dll[i][1],dll[i][2])
		if dll[i][1] >= remove:
			dll[i] = (dll[i][0], dll[i][1]-1,dll[i][2])
	#print(dll)
	dll.pop(remove)
	#print(remove)

#Derived from Primm
def phosphates(p_list, bucket):
	#print("This is p_list in phosphates {}".format(p_list))
	dll = [""]
	s_child = 0
	for mol in p_list:
		#(data, left, right) = struct.unpack("!LHH", mol)
		data = mol[0]
		left = mol[1]
		right = mol[2]
		if left == 0 or right == 0:
			s_child += 1
		dll.append((left, right, data))

	m_dict = {0: None}

	for i in range(1, len(dll)):
		if dll[i] != (0,0,0):
			m_dict[i] = (dll[i][0], dll[i][1], dll[i][2])

	for i in m_dict:
		if i:
			if m_dict[i][0] and m_dict[i][0] in m_dict.keys():
				if i not in m_dict[m_dict[i][0]] and m_dict[m_dict[i][0]]:
					return 0
			elif m_dict[i][1] and m_dict[i][1] in m_dict.keys():
				if i not in m_dict[m_dict[i][1]] and m_dict[m_dict[i][1]]:
					return 0
	p_list = clean_phosphates(m_dict, bucket)
	return p_list
	
#Derived from Primm
def clean_phosphates(dll, bucket):
	print("Found phosphates")
	ret_list = []
	ends = []
	for i in dll:
		if i:
			if 0 == dll[i][0] or 0 == dll[i][1]:
				ends.append(dll[i])

	if len(ends) == 0:
		return(ends)

	if ends[0][2] >= ends[1][2]:
		# rewire all nodes to chain link, pointed to the head, ends[0] is the new head
		head = ends[0]
	else:
		# rewire all nodes to chain link, pointed to the head, ends[1] is the new head
		head = ends[1]

	for i in dll:
		if i:
			if head == dll[i]:
				if head[0]:
					dll[i] = (head[0], head[0], head[2])
				else:
					dll[i] = (head[1], head[1], head[2])
				head = i
				break

	last = head
	while head:
		if dll[head][0] == 0 or dll[head][1] == 0:
			dll[head] = (0, 0, dll[head][2])
		elif dll[head][0] != last:
			dll[head] = (dll[head][0], dll[head][0], dll[head][2])
		else:
			dll[head] = (dll[head][1], dll[head][1], dll[head][2])
		last = head
		head = dll[head][0]
	
	for item in dll:
		ret_list.append((item[0], item[1], item[2]))
	return ret_list

def chlorine(p_list):
	change = 0
	ret_list = []
	for mol in p_list:
		left = mol[0]
		right = mol[1]
		data = mol[2]
		ret_list.append((left, right, data))
		(data, left, right) = mol[2],mol[0],mol[1]
		if left == right and left:
			right = 0
			ret_list.append((left, right, data))
			change = 1
		else:
			ret_list.append(mol)
	if change:
		return ret_list
	return 0

def chlorinate(data):
	if data[0]:
		data[1] = data[0]
	else:
		data[0] = data[1]
	return data

def lead(p_list, bucket):
	dll = []
	lead = 0
	#print("Here is the god dam p_list in lead {}".format(p_list))
	for mol in p_list:
		left = mol[0]
		right = mol[1]
		data = mol[2]
		if is_triangular(data):
			print("Found lead")
			bucket.add_waste(str(data))
			data = 0
			lead = 1
		else:
			pass

		dll.append((left, right, data))	

	if lead:
		return dll
	else:
		return 0

def main():
	num_worker_threads = 20

	server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

	#server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

	server.bind(('', 1111))	

	#server.listen(num_worker_threads)

	threads = []
	for i in range(num_worker_threads):
		t = threading.Thread(target=worker)
		t.start()
		threads.append(t)

	recieved = []	

	while server:
		try:
			if len(threads) < num_worker_threads:
				t = threading.Thread(target=worker)
				t.start()
				threads.append(t)

			try:
				conn, addr = server.recvfrom(1024)
			except Exception:
					break

			if conn: 
				recieved.append(conn)
				print("This is conn being appended to recieved list {}".format(conn))

			for item in recieved:
				q.put(item)
				recieved.remove(item)
		except Exception:
			# block until all tasks are done
			q.join()

			# stop workers
			for i in range(num_worker_threads):
				q.put(None)
			for t in threads:
				t.join()
	
if __name__ == "__main__":
	main()
